﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Data
{
    public class GameData : MonoBehaviour
    {
        private static bool loaded;

        [Header("Persisted in global scope")]
        public List<PersisteableEntity> Persisteable;
        
        [Header("Persisted in session scope")]
        public List<PersisteableEntity> LocalPersisteable;
        
        public GoldData Gold { 
            get { return (GoldData) Persisteable.Single(p => p.Entity is GoldData).Entity; }
        }
        
        public CrystalsData Crystals { 
            get { return (CrystalsData) Persisteable.Single(p => p.Entity is CrystalsData).Entity; }
        }

        public PointsData Points
        {
            get { return (PointsData) Persisteable.Single(p => p.Entity is PointsData).Entity; }
        }

        public T Find<T>() where T : ScriptableObject
        {
            T candidate = (T) Persisteable.FirstOrDefault(p => p.Entity is T).Entity;
            if (candidate == null)
                candidate = (T) LocalPersisteable.FirstOrDefault(p => p.Entity is T).Entity;
            return candidate;
        }
        
        public T Get<T>(string name) where T : ScriptableObject
        {
            PersisteableEntity candidate;
            candidate = Persisteable.FirstOrDefault(p => p.Name == name);
            if (candidate == null)
                candidate = LocalPersisteable.FirstOrDefault(p => p.Name == name);
            return (T) candidate.Entity;
        }

        void Awake()
        {
            if (!loaded)
            {
                #if UNITY_EDITOR
                #else
                foreach (PersisteableEntity pe in Persisteable){
                    GameDataPersister.Retrieve(ref pe.Entity, pe.Name);
                }
                #endif
                loaded = true;   
            }
        }
        
        public void RequestUpdate()
        {
            Gold.Init();
            Crystals.Init();
            Points.Init();
        }
        
        void OnDestroy()
        {
            foreach (PersisteableEntity pe in Persisteable){
                GameDataPersister.Store(pe.Entity, pe.Name);
            }
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if (pauseStatus)
            {
                foreach (PersisteableEntity pe in Persisteable){
                    GameDataPersister.Store(pe.Entity, pe.Name);
                }
            }
        }
    }
}
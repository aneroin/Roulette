﻿using System;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using Frame;
using UnityEngine;
using UnityEngine.Networking;

namespace Data
{
    public class GameDataPersister
    {
        private const string ServerUrl = "http://127.0.0.1:8080/GameDataPort/Platformer/";
        
        static byte[] bytes = ASCIIEncoding.ASCII.GetBytes(SystemInfo.deviceUniqueIdentifier);
        
        public static bool Store<T>(T data, string name)
        {
            #if UNITY_EDITOR
                if (!(data is ScriptableObject))
                    return false;
                
                UnityEditor.EditorUtility.SetDirty(data as ScriptableObject);
                    return true;
            #else
            try
            {
                string json = JsonUtility.ToJson(data);
                WriteFile(name, json);
                return true;
            }
            catch
            {
                return false;
            }
            #endif
        }

        public static bool Retrieve<T>(ref T data, string name)
        {
            try
            {
                string json = ReadFile(name);
                JsonUtility.FromJsonOverwrite(json, data);
                return true;
            }
            catch (IOException ioex)
            {
                Debug.LogError(ioex.Message);
                return false;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                return false;
            } 
        }
        
        
        public static bool StoreToServer<T>(T data, string name)
        {
            if (!(data is ScriptableObject))
                return false;
                
            try
            {
                #if UNITY_EDITOR
                    UnityEditor.EditorUtility.SetDirty(data as ScriptableObject);
                #endif
                
                string json = JsonUtility.ToJson(data);
                WriteToServer(name, json);
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                return false;
            } 

        }

        public static bool RetrieveFromServer<T>(ref T data, string name)
        {
            try
            {
                string json = null;
                
                ReadFromServer(name, (req) =>
                {
                    //TODO: Wait till request completed
                    json = req.downloadHandler.text;
                });
                
                if (json == null)
                    throw new Exception("No data received from server at ["+name+"]");
                
                JsonUtility.FromJsonOverwrite(json, data);
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.Message);
                return false;
            } 
        }

        static void WriteToServer(string name, string json)
        {
            var request = new UnityWebRequest(ServerUrl+name, "POST");
            byte[] bodyRaw = new System.Text.UTF8Encoding().GetBytes(json);
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            request.Send();
        }

        static void ReadFromServer(string name, Action<UnityWebRequest> reader)
        {
            UnityWebRequest request = UnityWebRequest.Get(ServerUrl+name);
            request.Send();
            reader(request);
        }

        static void WriteFile(string name, string json)
        {
            string filename = name + ".json";
        
        #if UNITY_EDITOR
            string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
        #else     
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);
        #endif
            
        File.WriteAllText (filePath, Wrap(json));
        }

        static string ReadFile(string name)
        {
            string filename = name + ".json";
            
        #if UNITY_EDITOR
            string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, filename);
        #else     
            string filePath = System.IO.Path.Combine(Application.persistentDataPath, filename);
        #endif
            
            try
            {
                if (File.Exists(filePath))
                {
                    string json = File.ReadAllText(filePath);
                    string datajson;
                    if (Unwrap(json, out datajson))
                        return datajson;
                    else
                        throw new IOException("file is not secure");
                    
                }
                else
                {
                    throw new IOException("no such data file");
                }
            }
            catch (Exception e)
            {
                throw new CryptographicException("can not decrypt the file: "+name+" "+e.Message);
            }
        }

        private static string Wrap(string json)
        {
            return JsonUtility.ToJson(new SignedEnvelope(json));
        }

        private static bool Unwrap(string cypher, out string json)
        {
            json = null;
            SignedEnvelope envelope = JsonUtility.FromJson<SignedEnvelope>(cypher);
            json = envelope.json;
            return SignedEnvelope.Sign(envelope.json) == envelope.sign;
        }

        private class SignedEnvelope
        {
            private static MD5 md5 = System.Security.Cryptography.MD5.Create();
            public string json;
            public string sign;
            public SignedEnvelope(string json)
            {
                this.json = json;
                this.sign = Sign(json);
            }

            public static string Sign(string json)
            {
                byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(json + SystemInfo.deviceUniqueIdentifier));
                return Encoding.UTF8.GetString(hash);    
            }
        }
    }
}
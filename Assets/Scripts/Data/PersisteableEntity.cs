﻿using System;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class PersisteableEntity
    {
        public string Name;
        public ScriptableObject Entity;
    }
}
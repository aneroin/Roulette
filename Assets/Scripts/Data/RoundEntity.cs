﻿using Frame.EventsPipe;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "GameData/Core/Round")]
    [System.Serializable] 
    public class RoundEntity : ScriptableObject
    {
        [SerializeField]
        protected int _turn;

        public int Turn
        {
            get { return _turn; }
            set { _turn = value % 2; PipeHost.SendEvent(new PipeEvent<int>(_turn, tag:"turn_changed"));}
        }

        [SerializeField]
        protected int bullets;

        public int Bullets
        {
            get { return bullets; }
        }
    }
}
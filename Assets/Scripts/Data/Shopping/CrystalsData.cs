﻿using System;
using System.Reflection;
using Frame.EventsPipe;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "GameData/Shopping/Crystals")]
    [System.Serializable] 
    public class CrystalsData : ScriptableObject
    {
        [SerializeField]
        private long _CrystalsAmount;

        public long CrystalsAmount
        {
            get { return _CrystalsAmount; }
        }
        
        static readonly object _block = new object();

        public void Init()
        {
            PipeHost.SendEvent(new PipeEvent<long>(_CrystalsAmount, tag: "crystals"));
        }

        public bool Pay(long amount)
        {
            lock (_block)
            {
                if (_CrystalsAmount < amount)
                {
                    return false;
                }
                else
                {
                    _CrystalsAmount -= amount;

                    PipeHost.SendEvent(new PipeEvent<long>(_CrystalsAmount, tag: "crystals"));
                    return true;
                }
            }
        }

        public void Earn(long amount)
        {
            lock (_block)
            {
                _CrystalsAmount += amount;
                PipeHost.SendEvent(new PipeEvent<long>(_CrystalsAmount, tag: "crystals"));
            }
        }
    }
}
﻿using System;
using System.Linq;
using System.Reflection;
using Frame.EventsPipe;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "GameData/Shopping/Gold")]
    [System.Serializable] 
    public class GoldData : ScriptableObject
    {
        [SerializeField]
        private long _GoldAmount;

        public long GoldAmount
        {
            get { return _GoldAmount; }
        }
        
        [NonSerialized]
        public Action<long> goldChanged;
        
        static readonly object _block = new object();

        public void Init()
        {
            PipeHost.SendEvent(new PipeEvent<long>(_GoldAmount, tag: "gold"));
        }
        
        public bool Pay(long amount)
        {
            lock (_block)
            {
                if (_GoldAmount < amount)
                {
                    return false;
                }
                else
                {
                    _GoldAmount -= amount;
                    PipeHost.SendEvent(new PipeEvent<long>(_GoldAmount, tag: "gold"));
                    return true;
                }
            }
        }

        public void Earn(long amount)
        {
            lock (_block)
            {
                _GoldAmount += amount;
                PipeHost.SendEvent(new PipeEvent<long>(_GoldAmount, tag: "gold"));
            }
        }
    }
}
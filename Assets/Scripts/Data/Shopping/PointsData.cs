﻿using System.Reflection;
using Frame.EventsPipe;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "GameData/Shopping/Points")]
    [System.Serializable]
    public class PointsData : ScriptableObject
    {
        [SerializeField] private long _PointsAmount;

        public long PointsAmount
        {
            get { return _PointsAmount; }
        }

        static readonly object _block = new object();

        public void Init()
        {
            PipeHost.SendEvent(new PipeEvent<long>(_PointsAmount, tag: "points"));
        }

        public bool Pay(long amount)
        {
            lock (_block)
            {
                if (_PointsAmount < amount)
                {
                    return false;
                }
                else
                {
                    _PointsAmount -= amount;

                    PipeHost.SendEvent(new PipeEvent<long>(_PointsAmount, tag: "points"));
                    return true;
                }
            }
        }

        public void Earn(long amount)
        {
            lock (_block)
            {
                _PointsAmount += amount;
                PipeHost.SendEvent(new PipeEvent<long>(_PointsAmount, tag: "points"));
            }
        }
    }
}
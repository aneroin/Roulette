﻿using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class Price
    {
        public enum Currency
        {
            Gold,
            Crystal,
            Real
        }

        [SerializeField]
        protected Currency _priceCurrency;

        public Currency PriceCurrency
        {
            get { return _priceCurrency; }
        }

        [SerializeField]
        protected long _priceAmount;

        public long PriceAmount
        {
            get { return _priceAmount; }
        }

        [SerializeField]
        protected string _priceCurrencyCode;

        public string PriceCurrencyCode
        {
            get { return _priceCurrencyCode; }
            set { _priceCurrencyCode = value; }
        }
    }
}
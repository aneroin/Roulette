﻿using System.Collections.Generic;

namespace Data
{
    [System.Serializable]
    public class ShoppingEntity
    {
        public string shopping_id;
        public string shopping_name;
        public string shopping_description;
     
        
        public int shopping_amount;
        public List<Price> shopping_prices;
    }
}
﻿using System;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class SoundEntity
    {
        public string name;
        public AudioClip clip;
    }
}
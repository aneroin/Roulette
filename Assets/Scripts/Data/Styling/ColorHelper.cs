﻿using System;
using UnityEngine;

namespace Data.Styling
{
    public static class ColorHelper
    {

        public struct ColorHSL
        {
            private float _h;

            public float H
            {
                get { return _h; }
                set
                {
                    float t = value;
                    t = t < 0f ? t += 1 : t;
                    _h = Mathf.Clamp01(value);
                }
            }

            private float _s;

            public float S
            {
                get { return _s; }
                set { _s = Mathf.Clamp01(value); }
            }

            private float _l;

            public float L
            {
                get { return _l; }
                set { _l = Mathf.Clamp01(value); }
            }

            private float _a;

            public float A
            {
                get { return _a; }
                set { _a = Mathf.Clamp01(value); }
            }

            public ColorHSL(float h, float s, float l)
            {
                _h = h;
                _s = s;
                _l = l;
                _a = 1f;
            }

            public ColorHSL(float h, float s, float l, float a)
            {
                _h = h;
                _s = s;
                _l = l;
                _a = a;
            }

        }

        public static ColorHSL RGBtoHSL(this Color color)
        {
            ColorHSL colorHsl = new ColorHSL(1f,1f,1f,1f);

            float min = Mathf.Min(color.r, color.g, color.b);
            float max = Mathf.Max(color.r, color.g, color.b);

            colorHsl.L = (min + max) / 2f;

            if (max == min)
            {
                colorHsl.H = colorHsl.S = 0;
            }
            else
            {
                float tH;
                float D = max - min;
                colorHsl.S = colorHsl.L < 0.5f ? D / (max+min) : D / (2f - max - min);

                if (max == color.r)
                    tH = (color.g - color.b) / (max - min);
                else if (max == color.g)
                    tH = 2f + (color.b - color.r) / (max - min);
                else
                    tH = 4f + (color.r - color.g) / (max - min);
                colorHsl.H = tH / 6f;
            }

            return colorHsl;
        }

        public static Color HSLtoRGB(this ColorHSL colorHsl)
        {
            Color colorRgb = new Color(1f,1f,1f,1f);

            if (colorHsl.S == 0)
            {
                colorRgb.r = colorRgb.g = colorRgb.b = 1f;
            }
            else
            {
                float q = colorHsl.L < 0.5
                    ? colorHsl.L * (1 + colorHsl.S)
                    : colorHsl.L + colorHsl.S - colorHsl.L * colorHsl.S;
                var p = 2 * colorHsl.L - q;

                colorRgb.r = hue2rgb(p, q, colorHsl.H + 1f / 3f);
                colorRgb.g = hue2rgb(p, q, colorHsl.H);
                colorRgb.b = hue2rgb(p, q, colorHsl.H - 1f / 3f);
            }

            return colorRgb;
        }

        private static float hue2rgb(float p, float q, float t)
        {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1f / 6f) return p + (q - p) * 6f * t;
            if (t < 1f / 2f) return q;
            if (t < 2f / 3f) return p + (q - p) * (2f / 3f - t) * 6f;
            return p;
        }

        public static Color Lighten(this Color color, float percentage)
        {
            ColorHSL colorHsl = color.RGBtoHSL();

            colorHsl.L += percentage;

            return colorHsl.HSLtoRGB();
        }

        public static Color Darken(this Color color, float percentage)
        {
            ColorHSL colorHsl = color.RGBtoHSL();

            colorHsl.L -= percentage;

            return colorHsl.HSLtoRGB();
        }

        public static Color Vivid(this Color color, float percentage)
        {
            ColorHSL colorHsl = color.RGBtoHSL();

            colorHsl.S += percentage;
            
            return colorHsl.HSLtoRGB();
        }

        public static Color Deaf(this Color color, float percentage)
        {
            ColorHSL colorHsl = color.RGBtoHSL();

            colorHsl.S -= percentage;

            return colorHsl.HSLtoRGB();
        }
    }

}
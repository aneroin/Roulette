﻿using System;
using System.Reflection;
using Frame;
using UnityEngine;

namespace Data.Styling
{
    public class Styler : MonoBehaviour
    {
        [SerializeField] protected string Resource;
        [SerializeField] protected MonoBehaviour Target;
        [SerializeField] protected string TargetPropertyname = "color";
        
        [Range(-1f, 1f)]
        public float Luminosity;
        [Range(-1f, 1f)]
        public float Saturation;
        [Range(-1f, 1f)]
        public float Transparency;

        void Start()
        {
            UISkin skin = GameScope.Get<GameData>().Get<UISkin>("main_skin");
            
            PropertyInfo colorGetter = skin.GetType().GetProperty(Resource);
            Type t = skin.GetType();
            PropertyInfo colorTarget = Target.GetType().GetProperty(TargetPropertyname);
            t = Target.GetType();

            if (colorGetter != null && colorTarget != null)
            {
                Color resourceColor = (Color) (colorGetter.GetValue(skin, null) ?? Color.magenta);

                if (Luminosity != 0f)
                {
                    if (Luminosity > 0f)
                    {
                        resourceColor = resourceColor.Lighten(Luminosity);
                    }

                    if (Luminosity < 0f)
                    {
                        resourceColor = resourceColor.Darken(Luminosity);
                    }
                }

                if (Saturation != 0f)
                {
                    if (Saturation > 0f)
                    {
                        resourceColor = resourceColor.Vivid(Saturation);
                    }

                    if (Saturation < 0f)
                    {
                        resourceColor = resourceColor.Deaf(Saturation);
                    }
                }

                if (Transparency != 0f)
                {
                    resourceColor.a = Mathf.Clamp01(resourceColor.a + Transparency);
                }

                colorTarget.SetValue(Target, resourceColor, null);
            }
            else
            {
                Debug.LogErrorFormat("color resource is {0} and color target is {1}", colorGetter, colorTarget);
            }
        }

    }
}
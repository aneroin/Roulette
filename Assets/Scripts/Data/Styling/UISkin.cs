﻿using UnityEngine;

namespace Data.Styling
{
    [CreateAssetMenu(menuName = "GameData/UI/Skin")]
    [System.Serializable] 
    public class UISkin : ScriptableObject
    {
        [SerializeField] protected Color primaryColor;

        public Color PrimaryColor
        {
            get { return primaryColor; }
        }

        
        [SerializeField] protected Color secondaryColor;

        public Color SecondaryColor
        {
            get { return secondaryColor; }
        }

        
        [SerializeField] protected Color complementaryColor;

        public Color ComplementaryColor
        {
            get { return complementaryColor; }
        }

        

        [SerializeField] protected Color typeColorCommon;

        public Color TypeColorCommon
        {
            get { return typeColorCommon; }
        }

        
        [SerializeField] protected Color typeColorAttention;

        public Color TypeColorAttention
        {
            get { return typeColorAttention; }
        }

        
        [SerializeField] protected Color typeColorSuccess;

        public Color TypeColorSuccess
        {
            get { return typeColorSuccess; }
        }

        
        [SerializeField] protected Color typeColorFail;

        public Color TypeColorFail
        {
            get { return typeColorFail; }
        }

        

        [System.Serializable]
        public class SkinColor
        {
            [SerializeField] protected string name;
            [SerializeField] protected Color color;
        }
    }
}
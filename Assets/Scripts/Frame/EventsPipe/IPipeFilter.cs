﻿using System;

namespace Frame.EventsPipe
{
    public interface IPipeFilter<T> : IPipeFilterStub
    {
        void Filter(PipeEvent<T> pevent);
        bool Evaluate();
    }
}
﻿using System;

namespace Frame.EventsPipe
{
    public interface IPipeFilterStub
    {
        Type FilterType();
        Type InnerType();
    }
}
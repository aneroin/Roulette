﻿namespace Frame.EventsPipe
{
    public interface IPipeInterceptor : IPipeInterceptorStub
    {
        void Intercept(PipeEventBase pevent);
    }
}
﻿using System;

namespace Frame.EventsPipe
{
    public interface IPipeInterceptorStub
    {
        Type InterceptorType();
    }
}
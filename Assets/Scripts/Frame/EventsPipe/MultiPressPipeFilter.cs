﻿using System;
using System.Collections.Generic;

namespace Frame.EventsPipe
{
    public class MultiPressPipeFilter<T> : PipeFilter<T>
    {
        private List<EventNode<T>> _sequence = new List<EventNode<T>>();

        private long _timespan;
        private T _data;
        private string _tag;
        private int _pressCount;
        
        public MultiPressPipeFilter(int presses = 2, float timespanSeconds = 1f, T data = default(T), string tag = null)
        {
            _pressCount = presses;
            _timespan = (long)(timespanSeconds * 10000000);
            _data = data;
            _tag = tag;
        }
        
        public MultiPressPipeFilter(int presses = 2, long timespanTicks = 10000000, T data = default(T), string tag = null)
        {
            _pressCount = presses;
            _timespan = timespanTicks;
            _data = data;
            _tag = tag;
        }
        
        public void Filter(PipeEvent<T> pevent)
        {
            if (pevent == null)
                return;

            if (_data != null && !_data.Equals(pevent.Data))
                return;

            if (_tag != null && _tag != pevent.Tag)
                return;
            
            EventNode<T> en = new EventNode<T>(pevent);
            for (int i = 0; i < _sequence.Count; i++)
            {
                if (en.Timestamp - _sequence[i].Timestamp > _timespan)
                    _sequence.RemoveAt(i);
            }
            _sequence.Add(en);
        }
        
        public bool Evaluate()
        {
            bool response = _sequence.Count == _pressCount;
            if (response)
                _sequence.Clear();
            return response;
        }
    }

    class EventNode<T>
    {
        private PipeEvent<T> node;

        public PipeEvent<T> Node
        {
            get { return node; }
            set { node = value; }
        }
        
        private long timestamp;

        public long Timestamp
        {
            get { return timestamp; }
        }

        public EventNode(PipeEvent<T> pevent)
        {
            node = pevent;
            timestamp = DateTime.Now.Ticks;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Frame.EventsPipe;
using UnityEngine;

namespace Frame.EventsPipe.PipeDebug
{
    public class DebugPipeFilter<T> : IPipeFilter<T>
    {
        private int _lastQueryCount = 0;
        private bool _response;

        private string _tag = null;

        public string tag
        {
            get { return _tag; }
            set { _tag = value; }
        }
        
        private List<PipeEvent<T>> _eventsStack = new List<PipeEvent<T>>();

        public List<PipeEvent<T>> EventsStack
        {
            get { return _eventsStack; }
        }
        
        
        public void Filter(PipeEvent<T> pevent)
        {
            if (_tag != null && pevent.Tag != _tag)
                    return;
            _eventsStack.Add(pevent);
        }

        public bool Evaluate()
        {
            _response = _lastQueryCount != _eventsStack.Count;
            _lastQueryCount = _eventsStack.Count;
            return _response;
        }

        public Type FilterType()
        {
            return typeof(DebugPipeFilter<T>);
        }

        public Type InnerType()
        {
            return (typeof(T));
        }
    }
}
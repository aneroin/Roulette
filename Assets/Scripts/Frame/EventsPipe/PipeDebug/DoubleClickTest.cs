﻿using UnityEngine;

namespace Frame.EventsPipe.PipeDebug
{
    public class DoubleClickTest : MonoBehaviour
    {
        MultiPressPipeFilter<int> doubleClick6 = new MultiPressPipeFilter<int>(presses:2, timespanSeconds:1f, data: 6, tag:"ui_thread");

        void Start()
        {
            PipeHost.SubscribeFilter(doubleClick6);
        }

        private void FixedUpdate()
        {
            if (doubleClick6.Evaluate())
            {
                Debug.LogWarning("DOUBLE CLICKED 6!!!");
            }
        }
    }
}
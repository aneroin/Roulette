﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Frame.EventsPipe.PipeDebug
{
    public class PipeEventsRenderer : MonoBehaviour
    {
        public Text OutputMessageText;
        public Text OutputValueText;
        
        [SerializeField] protected int RenderCount;
        protected List<PipeEvent<String>> MessagesStack;
        protected List<PipeEvent<int>> ValuesStack;
        
        readonly DebugPipeFilter<String> _stackMessagesFilter = new DebugPipeFilter<String>();
        readonly DebugPipeFilter<int> _stackValuesFilter = new DebugPipeFilter<int>();
        
        private void Start()
        {
            MessagesStack = new List<PipeEvent<String>>(RenderCount);
            ValuesStack = new List<PipeEvent<int>>(RenderCount);
            
            PipeHost.SubscribeFilter(_stackMessagesFilter);
            PipeHost.SubscribeFilter(_stackValuesFilter);
            
            PipeHost.Subscribe<String>((e) => { return; }, "ui_thread");
            PipeHost.Subscribe<String>((e) => { return; }, "game_thread");
            PipeHost.Subscribe<int>((e) => { return; }, "ui_thread");
            PipeHost.Subscribe<int>((e) => { return; }, "game_thread");
        }

        private void FixedUpdate()
        {
            if (_stackMessagesFilter.Evaluate())
            {
                MessagesStack = _stackMessagesFilter.EventsStack.Skip(Mathf.Max(0, _stackMessagesFilter.EventsStack.Count - RenderCount)).ToList();
                RenderStack(MessagesStack);
            }
            
            if (_stackValuesFilter.Evaluate())
            {
                ValuesStack = _stackValuesFilter.EventsStack.Skip(Mathf.Max(0, _stackValuesFilter.EventsStack.Count - RenderCount)).ToList();
                RenderStack(ValuesStack);
            }
        }

        private void RenderStack(List<PipeEvent<String>> stack)
        {
            if (stack == null)
                return;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("\r\n Events Pipe: [");
            foreach (var evt in stack)
            {
                if (evt != null)
                    sb.Append(String.Format("\n\t tag: {0} \r\n\t data:{1} \t", evt.Tag, evt.Data));
                else
                    sb.Append(" `null` ");
            }
            sb.Append("\r\n]");
            
            if (OutputMessageText != null)
            {
                OutputMessageText.text = sb.ToString();
            }
        }
        
        private void RenderStack(List<PipeEvent<int>> stack)
        {
            if (stack == null)
                return;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("\r\n Events Pipe: [");
            foreach (var evt in stack)
            {
                if (evt != null)
                    sb.Append(String.Format("\n\t tag: {0} \r\n\t data:{1} \t", evt.Tag, evt.Data));
                else
                    sb.Append(" `null` ");
            }
            sb.Append("\r\n]");
            
            if (OutputValueText != null)
            {
                OutputValueText.text = sb.ToString();
            }
        }
    }
}
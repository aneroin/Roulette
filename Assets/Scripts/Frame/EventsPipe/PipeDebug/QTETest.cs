﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Frame.EventsPipe.PipeDebug
{
    public class QTETest : MonoBehaviour
    {
        private bool working;
        [SerializeField] private List<KeyCode> QTE_Sequence;
        [SerializeField] private int Tolerance;
        private int currentKey = 0;
        private QTEPipeFilter QteFilter;

        private Coroutine TimeConstraintCotoutine;

        void Start()
        {
            QteFilter = new QTEPipeFilter(
                sequence: QTE_Sequence, tolerance: Tolerance, 
                tag: "input_keyboard", 
                stageCallback:QteStageCallback, finalCallback:QteFinalCallback);
            
            PipeHost.SubscribeFilter(QteFilter);
            Debug.Log("QTE START!");
            Debug.Log("[ " + QTE_Sequence[0] + " ]");
            
            if (TimeConstraintCotoutine != null)
                StopCoroutine(TimeConstraintCotoutine);
            TimeConstraintCotoutine = StartCoroutine(TimeConstraint(1f, () => { QteFilter.ForceNext(); }));
            
            working = true;
        }

        private void Update()
        {
            if (working)
            {
                foreach (var key in QTE_Sequence)
                {
                    if (!working)
                        return;
                    
                    if (Input.GetKeyDown(key))
                    {
                        PipeHost.SendEvent(new PipeEvent<KeyCode>(data: key, tag: "input_keyboard"), silent:false);
                    }
                }
            }
        }

        void QteStageCallback(bool result)
        {
            currentKey++;

            if (currentKey == QTE_Sequence.Count)
            {
                QteFilter.ForceNext();
                return;
            }

            Debug.Log("[ " + QTE_Sequence[currentKey] + " ]");
            
            if (TimeConstraintCotoutine != null)
                StopCoroutine(TimeConstraintCotoutine);
            TimeConstraintCotoutine = StartCoroutine(TimeConstraint(1f, () => { QteFilter.ForceNext(); }));
        }

        void QteFinalCallback(bool result)
        {
            working = false;
            if (TimeConstraintCotoutine != null)
                StopCoroutine(TimeConstraintCotoutine);
            Debug.Log("QTE IS "+(result ? "FINISHED" : "RUINED"));
            StartCoroutine(TimeConstraint(0.1f, () => {PipeHost.UnsubscribeFilter(QteFilter);}));
            
        }

        IEnumerator TimeConstraint(float t, Action call)
        {
            yield return new WaitForSeconds(t);
            call();
        }
        
    }
}
﻿using System;
using UnityEngine;

namespace Frame.EventsPipe.PipeDebug
{
    public class SendPipeMessageComponent : MonoBehaviour
    {
        public string tag;

        public void SendPipeMessage(string message)
        {
            PipeHost.SendEvent(new PipeEvent<string>(tag: tag, data: message));
        }
        
        public void SendPipeMessage(int value)
        {
            PipeHost.SendEvent(new PipeEvent<int>(tag: tag, data: value));
        }
    }
}
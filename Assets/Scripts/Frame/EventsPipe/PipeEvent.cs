﻿using System;
using UnityEngine;

namespace Frame.EventsPipe
{
    [System.Serializable]
    public class PipeEvent<T> : PipeEventBase
    {
        public T Data
        {
            get { return (T) data_raw; }
        }
        
        public PipeEvent(T data, string tag = null)
        {
            this.data_raw = data;
            this.innerType = typeof(T);
            this.tag = tag ?? String.Empty;
        }
    }
}
﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Frame.EventsPipe
{
    [System.Serializable]
    public class PipeEventBase
    {
        static BinaryFormatter bf = new BinaryFormatter();
        
        [SerializeField]
        protected string tag;
        [SerializeField]
        protected object data_raw;
        [SerializeField]
        protected Type innerType;

        public string Tag
        {
            get { return tag; }
        }
        
        public object DataRaw
        {
            get { return data_raw; }
        }
        
        public Type InnerType
        {
            get { return innerType; }
        }
        
        public byte[] ToBytes()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        public static PipeEventBase ToPipeEventBase(byte[] input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(input, 0, input.Length);
                ms.Seek(0, SeekOrigin.Begin);
                return (PipeEventBase) bf.Deserialize(ms);
            }
        }
    }
}
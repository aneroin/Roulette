﻿using System;

namespace Frame.EventsPipe
{
    public abstract class PipeFilter<T> : IPipeFilter<T>
    {
        public virtual Type FilterType()
        {
            return this.GetType();
        }

        public virtual Type InnerType()
        {
            return typeof(T);
        }

        public virtual void Filter(PipeEvent<T> pevent)
        {
            throw new NotImplementedException();
        }

        public virtual bool Evaluate()
        {
            throw new NotImplementedException();
        }
    }
}
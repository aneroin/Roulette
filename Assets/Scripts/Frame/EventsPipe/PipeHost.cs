﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Frame.EventsPipe
{
    /// <summary>
    /// Pipe Event Dispatcher Host
    /// Should be placed in the game scope
    /// Should be single-per-game
    /// Can serve as a loose-coupling mechanism between Model and View
    /// </summary>
    public class PipeHost : MonoBehaviour
    {

        public static bool verbose = false;
        
        private static List<IPipeInterceptor> _interceptorsStack = new List<IPipeInterceptor>();
        
        private static List<IPipeFilterStub> _filtersStack = new List<IPipeFilterStub>();
        
        /// <summary>
        /// Events dictionary
        /// Each TagType key maps event to the list of listeners
        /// </summary>
        private static Dictionary<TagTypeKey, List<Delegate>> PipeStack = new Dictionary<TagTypeKey, List<Delegate>>();


        public static void SubscribeInterceptor(IPipeInterceptor interceptor)
        {
            _interceptorsStack.Add(interceptor);
        }

        public static void UnsubscribeInterceptor(IPipeInterceptor interceptor)
        {
            _interceptorsStack.Remove(interceptor);
        }
        
        
        public static void SubscribeFilter<T>(IPipeFilter<T> filter)
        {
            _filtersStack.Add(filter);
        }
        
        public static void UnsubscribeFilter<T>(IPipeFilter<T> filter)
        {
            _filtersStack.Remove(filter);
        }
        
        /// <summary>
        /// Subscribe listener to the pipe
        /// </summary>
        /// <param name="callback">event handler to call</param>
        /// <param name="tag">optional tag to filter events</param>
        /// <typeparam name="T">type of data this event sends</typeparam>
        public static void Subscribe<T>(Action<T> callback, string tag = null)
        {
            Type dataType = typeof(T);
            TagTypeKey ttkey = new TagTypeKey(dataType, tag ?? String.Empty);
            
            if (!PipeStack.ContainsKey(ttkey))
            {
                PipeStack.Add(ttkey, new List<Delegate>());
            }
            PipeStack[ttkey].Add(callback);
        }

        /// <summary>
        /// Unsubscribe listener from the pipe
        /// </summary>
        /// <param name="callback">event handler</param>
        /// <param name="tag">optional tag</param>
        /// <typeparam name="T">type of data this event handler consumes</typeparam>
        public static void Unsubscribe<T>(Action<T> callback, string tag = null)
        {
            if (tag == null)
            {
                foreach (var capillar in PipeStack)
                {
                    capillar.Value.RemoveAll(e => e == (Delegate) callback);
                }
            }
            else
            {
                PipeStack[new TagTypeKey(typeof(T), tag)].RemoveAll(e => e == (Delegate) callback);
            }
        }
        
        /// <summary>
        /// Send PipeEvent to the pipe
        /// </summary>
        /// <param name="pipeEvent">event with data and optional tag</param>
        /// <typeparam name="T">type of data</typeparam>
        public static void SendEvent<T>(PipeEvent<T> pipeEvent, bool silent = false)
        {
            Type dataType = typeof(T);
            TagTypeKey ttkey = new TagTypeKey(dataType, pipeEvent.Tag ?? String.Empty);
            
            if (!silent)
            foreach (var interceptor in _interceptorsStack)
            {
                if (interceptor != null)
                    {
                        try
                        {
                            interceptor.Intercept(pipeEvent);
                        }
                        catch (Exception e)
                        {
                            if (verbose) 
                                Debug.LogWarningFormat("{0} interceptor has failed to handle handle {1} data", interceptor.GetType(), pipeEvent.GetType());
                        }
                    }
            }
            
            foreach (var filter in _filtersStack)
            {
                if (filter != null)
                {
                    try
                    {
                        filter.FilterType().GetMethod("Filter")
                            .Invoke(filter, new object[] {pipeEvent});
                    }
                    catch (Exception e)
                    {
                        if (verbose)
                            Debug.LogWarningFormat("{0} can not handle {1} data", filter.FilterType(), pipeEvent.GetType());
                    }
                }
            }

            if (PipeStack.ContainsKey(ttkey))
            {
                List<Delegate> callList = PipeStack[ttkey];

                for (int i = callList.Count - 1; i >= 0; i--)
                {
                    if (callList != null)
                        callList[i].DynamicInvoke(pipeEvent.Data);
                }
            }
        }
        
        //FIX: OBSOLETE 
        /*
        /// <summary>
        /// Send data as event
        /// </summary>
        /// <param name="data">event data</param>
        /// <param name="tag">optional event tag</param>
        /// <typeparam name="T">type of data</typeparam>
        public static void SendEvent<T>(T data, string tag = null)
        {
            Type dataType = typeof(T);
            TagTypeKey ttkey = new TagTypeKey(dataType, tag ?? String.Empty);

            Type eventType = typeof(PipeEvent<T>).MakeGenericType(data.GetType());
            object generatedPevent = Activator.CreateInstance(eventType, new object[] {data, tag});
            
            foreach (var filter in _filtersStack)
            {
                if (filter != null)
                {
                    try
                    {
                        filter.FilterType().GetMethod("Filter")
                            .Invoke(filter, new object[] { generatedPevent });
                    }
                    catch (Exception e)
                    {
                        if (verbose)
                            Debug.LogWarningFormat("{0} can not handle {1} data", filter.FilterType(), generatedPevent.GetType());
                    }
                }
            }
            
            if (PipeStack.ContainsKey(ttkey))
            {
                List<Delegate> callList = PipeStack[ttkey];
                for (int i = callList.Count - 1; i >= 0; i--)
                {
                    if (callList != null)
                        callList[i].DynamicInvoke(data);
                }
            }
            else
                if (verbose)
                    Debug.LogWarningFormat("PipeHost: there are no event subscribers for tag {0} of type {1}", ttkey.tag, ttkey.type);
        }
        
        */

        /// <summary>
        /// Compound Key for dictionary, contains information about Tag and Type of event
        /// </summary>
        protected class TagTypeKey
        {
            /// <summary>
            /// simple string tag, may be omitted
            /// </summary>
            public string tag;
            /// <summary>
            /// type to dispatch events
            /// </summary>
            public Type type;

            public TagTypeKey(Type type, string tag)
            {
                this.tag = tag;
                this.type = type;
            }

            public override bool Equals(object obj)
            {
                if (obj is TagTypeKey)
                {
                    var ttkey = (TagTypeKey) obj;
                    return this.tag.Equals(ttkey.tag) && this.type.Equals(ttkey.type);
                }

                return false;
            }
            
            public override int GetHashCode()
            {
                return this.tag.GetHashCode() ^ this.type.GetHashCode();
            }
        }
    }
}
﻿using System;

namespace Frame.EventsPipe
{
    public class PipeInterceptor : IPipeInterceptor
    {
        public virtual Type InterceptorType()
        {
            return this.GetType();
        }

        public virtual void Intercept(PipeEventBase pevent)
        {
            throw new NotImplementedException();
        }
    }
}
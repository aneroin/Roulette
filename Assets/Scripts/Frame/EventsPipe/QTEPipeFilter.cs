﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Frame.EventsPipe
{
    public class QTEPipeFilter : PipeFilter<KeyCode>
    {
        private string _tag;
        private List<KeyCode>.Enumerator _enumerator;
        protected List<KeyCode> qte_sequence;
        protected Action<bool> qte_stage_callback;
        protected Action<bool> qte_final_callback;
        protected int failed_parts;
        protected int fail_tolerance;
        protected bool finished;

        public QTEPipeFilter(IEnumerable<KeyCode> sequence, int tolerance = 0,  string tag = null, Action<bool> stageCallback = null, Action<bool> finalCallback = null)
        {
            _tag = tag;
            failed_parts = 0;
            fail_tolerance = tolerance;
            qte_sequence = new List<KeyCode>(sequence);
            
            qte_stage_callback = stageCallback;
            qte_final_callback = finalCallback;
            
            if (fail_tolerance >= qte_sequence.Count)
            {
                throw new ArgumentOutOfRangeException(String.Format("Fail tolerance of {0} is greater or equal to the size of QTE sequence, which makes this sequence useless", fail_tolerance));
            }
            _enumerator = qte_sequence.GetEnumerator();
        }

        public void ForceNext()
        {
            if (_enumerator.MoveNext())
            {
                failed_parts++;
                if (qte_stage_callback != null)
                {
                    qte_stage_callback(false);
                }
            } else {
                finished = true;
                
                Debug.Log("Qte Failed parts "+failed_parts+" with tolerance of "+fail_tolerance);
                
                if (qte_final_callback != null)
                {
                    qte_final_callback(failed_parts <= fail_tolerance);
                }
            }
        }

        public void Filter(PipeEvent<KeyCode> pevent)
        {
            if (pevent == null)
                return;

            if (_tag != null && _tag != pevent.Tag)
                return;

            if (_enumerator.MoveNext())
            {
                if (_enumerator.Current == pevent.Data)
                {
                    if (qte_stage_callback != null)
                    {
                        qte_stage_callback(true);
                    }
                }
                else
                {
                    failed_parts++;
                    
                    if (qte_stage_callback != null)
                    {
                        qte_stage_callback(false);
                    }
                }
            }
            else
            {
                finished = true;
                
                Debug.Log("Qte Failed parts "+failed_parts+" with tolerance of "+fail_tolerance);
                
                if (qte_final_callback != null)
                {
                    qte_final_callback(failed_parts <= fail_tolerance);
                }
            }
        }

        public bool Evaluate()
        {
            return failed_parts <= fail_tolerance;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using Frame;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    Previous,
    Welcome,
    Spinners,
    Shop,
    Campaign,
    PreMatch,
    Match,
    PostMatch,
    Reward
}

public class GameController : MonoBehaviour
{   
    private Stack<GameState> navigationHistory = new Stack<GameState>();
    
    public GameState _state = GameState.Welcome;
    public GameState _previousState; 
    public GameState State
    {
        get { return _state; }
        set
        {

            // if requested state is Previous, request back link from current state, else pass value directly
            GameState _nextState = value == GameState.Previous ? BackFrom(_state, _previousState) : value;
            
            // If requested state is not same as current one, write navigation step
            if (_state != _nextState)
            {
                navigationHistory.Push(_state);
                _previousState = _state;
            }
            
            // store current state
            _state = _nextState;
                 
            // call transfer function
            if (Switches.ContainsKey(_state))
                Switches[_state]();
        }
    }

    private Dictionary<GameState, Action> Switches;


    void Awake()
    {
        Switches = new Dictionary<GameState, Action>()
        {
            {GameState.Previous, SwitchBack},
            {GameState.Welcome, SwitchWelcome},
            {GameState.Spinners, SwitchSpinners},
            {GameState.Shop, SwitchShop},
            {GameState.Campaign, SwitchCampaign},
            {GameState.PreMatch, SwitchPreMatch},
            {GameState.Match, SwitchMatch},
            {GameState.PostMatch, SwitchPostMatch},
            {GameState.Reward, SwitchReward}
        };
        
        BuildNavigationFrom(_state);
        SceneManager.sceneLoaded += ToonChange;
    }

    void SwitchWelcome()
    {
        SceneManager.LoadScene("scene_welcome");
    }
    
    void SwitchSpinners()
    {
        SceneManager.LoadScene("scene_spinners");
        PlayerPrefs.SetInt("Col_New",0);
    }

    void SwitchShop()
    {
        SceneManager.LoadScene("scene_shop");
    }

    void SwitchCampaign()
    {
        GameScope.Instance.GetComponent<RandomBroker>().ToInitialState("Tree");
        GameScope.Instance.GetComponent<RandomBroker>().ToInitialState("House");
        SceneManager.LoadScene("scene_campaign");
    }

    void SwitchPreMatch()
    {
        SceneManager.LoadScene("scene_prematch");
    }
    
    void SwitchMatch()
    {
        SceneManager.LoadScene("scene_match");
    }

    void SwitchPostMatch()
    {
        SceneManager.LoadScene("scene_postmatch");
    }

    void SwitchReward()
    {
        SceneManager.LoadScene("scene_reward");
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SwitchBack();
        }
    }

    void SwitchBack()
    {
        Switches[BackFrom(_state, _previousState)]();
        /*
        if (navigationHistory.Count > 0)
            State = navigationHistory.Pop();*/
    }

    void BuildNavigationFrom(GameState state)
    {
        switch (state)
        {
            case GameState.Shop:
            {
                navigationHistory.Push(GameState.Welcome);
                navigationHistory.Push(GameState.Spinners);
                break;
            }
                
            case GameState.Spinners:
            {
                navigationHistory.Push(GameState.Welcome);
                break;
            }
             
            case GameState.Campaign:
            {
                navigationHistory.Push(GameState.Welcome);
                break;
            }
                
            case GameState.PreMatch:
            {
                navigationHistory.Push(GameState.Welcome);
                navigationHistory.Push(GameState.Campaign);
                break;
            }
                
            case GameState.Match:
            {
                navigationHistory.Push(GameState.Welcome);
                navigationHistory.Push(GameState.Campaign);
                navigationHistory.Push(GameState.PreMatch);
                break;
            }
                
            case GameState.PostMatch:
            {
                navigationHistory.Push(GameState.Welcome);
                navigationHistory.Push(GameState.Campaign);
                navigationHistory.Push(GameState.PreMatch);
                break;
            }
                
            case GameState.Reward:
            {
                navigationHistory.Push(GameState.Welcome);
                navigationHistory.Push(GameState.Campaign);
                break;
            }
        }
    }

    /// <summary>
    /// Here all the mappings will be stored, and back button will be handled here based on _state and _previousState context
    /// </summary>
    /// <param name="state"></param>
    /// <param name="previousState"></param>
    /// <returns></returns>
    GameState BackFrom(GameState state, GameState previousState)
    {
        if (state == GameState.Shop)
        {
            return GameState.Spinners;
        }
        
        if (state == GameState.Spinners)
        {
            if (_previousState == GameState.Campaign)
                return GameState.Campaign;
            if (_previousState == GameState.PreMatch)
                return GameState.PreMatch;

            return GameState.Welcome;
        }

        if (state == GameState.Campaign)
        {
            if (previousState == GameState.Spinners)
            {
                return GameState.Spinners;
            }
            if (previousState == GameState.PreMatch)
            {
                return GameState.PreMatch;
            }
        }
        
        if (state == GameState.PreMatch)
        {
            return GameState.Campaign;
        }
        
        if (state == GameState.PostMatch)
        {
            return GameState.PreMatch;
        }

        return GameState.Welcome;
    }

    void ToonChange(Scene scene, LoadSceneMode mode)
    {
        switch (scene.name)
        {
            case "scene_welcome":
            {
                SoundManager.Play("background1", true, 1f, SoundManager.MAX_BG_VOLUME);
                break;
            }
                
            case "scene_campaign":
            {
                SoundManager.Play("background2", true, 1f, SoundManager.MAX_BG_VOLUME);
                break;
            }
                
            case "scene_prematch":
            {
                SoundManager.Play("background3", true, 1f, SoundManager.MAX_BG_VOLUME);
                break;
            }
                
            case "scene_match":
            {
                SoundManager.Play("background4", true, 1f, SoundManager.MAX_BG_VOLUME);
                break;
            }   
                
            case "scene_postmatch":
            {
                SoundManager.Play("background5", true, 1f, SoundManager.MAX_BG_VOLUME);
                break;
            } 
                
            case "scene_reward":
            {
                SoundManager.Play("background6", true, 1f, SoundManager.MAX_BG_VOLUME);
                break;
            }
        }
    }
}

﻿using UnityEngine;

namespace Frame
{   
    public class GameScope : MonoBehaviour
    {
        [Header("This object will be single in a whole game scope")]
        public bool iHaveReadThis;
        
        private static GameScope _Instance;

        public static GameScope Instance
        {
            get { return _Instance; }
        }

        void Awake()
        {
            if (_Instance != null)
            {
                if (_Instance != this)
                    DestroyImmediate(this.gameObject);
            }
            else
            {
                _Instance = this;
                DontDestroyOnLoad(this.gameObject);
                foreach (var component in GetComponents<MonoBehaviour>())
                {
                    DontDestroyOnLoad(component);
                }
            }
        }

        public static T Get<T>()
        {
            if (_Instance == null)
            {
                Debug.LogError("There are no GameScope game object on a scene");
                return default(T);
            }
         
            return _Instance.GetComponent<T>();
        }
    }
}
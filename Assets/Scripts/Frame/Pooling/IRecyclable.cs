﻿namespace Frame.Pooling
{
    /// <summary>
    /// Recyclable object
    /// Declares methods for recycling and refreshing
    /// Declares methods for pool interaction
    /// </summary>
    public interface IRecyclable
    {
        Pool GetPool();
        void SetPool(Pool pool);
        
        void Recycle();
        void Refresh();
    }
}
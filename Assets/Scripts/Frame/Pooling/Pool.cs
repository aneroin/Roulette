﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Frame.Pooling
{
    public class Pool
    {       
        /// <summary>
        /// for better output and navigation
        /// </summary>
        public string name;

        /// <summary>
        /// unique identifier
        /// </summary>
        protected string id;

        /// <summary>
        /// ID Accessor
        /// </summary>
        /// <returns>id</returns>
        public string ID() { return id; }

        /// <summary>
        /// actual container of the objects
        /// TODO: Consider usage of stacks or queues
        /// </summary>
        private List<IRecyclable> pool = new List<IRecyclable>();
        
        /// <summary>
        /// Constructor, passed to the pool
        /// </summary>
        private Func<IRecyclable> producer;
        
        /// <summary>
        /// Destructor, passed to the pool
        /// </summary>
        private Action<IRecyclable> destroyer;
        
        /// <summary>
        /// Pool constructor
        /// </summary>
        /// <param name="name">optional name</param>
        /// <param name="producer">optional constructor</param>
        /// <param name="destroyer">optional destructor</param>
        public Pool(string name = "global", Func<IRecyclable> producer = null, Action<IRecyclable> destroyer = null)
        {
            this.name = name;
            this.producer = producer;
            this.destroyer = destroyer;
            id = System.Guid.NewGuid().ToString();
        }
        
        /// <summary>
        /// Safe get instance of the IRecyclable object from pool
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public bool TryGet(out IRecyclable instance)
        {
            IRecyclable recyclable = pool.FirstOrDefault();
            if (recyclable != null)
            {
                pool.Remove(recyclable);
                recyclable.Refresh();
                instance = recyclable;
                return true;
            }
            else if (producer != null)
            {

                recyclable = producer();
                recyclable.SetPool(this);
                recyclable.Refresh();
                instance = recyclable;
                return true;
            }
            else
            {
                instance = default(IRecyclable);
                return false;
            }
        }
        
        /// <summary>
        /// Get IRecyclable object from pool, casted to the type T
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <returns>returns object of type or default</returns>
        public T Get<T>()
        {
            // if type T implements IRecyclable - it may be stored in this pool
            if (typeof(IRecyclable).IsAssignableFrom(typeof(T)))
                return (T) Get();
            // otherwise return default value of T and drop some error in log
            else
            {
               Debug.LogError("Can not emit "+typeof(T).Name+" from "+name+":["+id+"] Pool");
                return (T) default(IRecyclable);
            }
        }
        
        /// <summary>
        /// Get IRecyclable object from pool
        /// </summary>
        /// <returns></returns>
        public IRecyclable Get()
        {
            // Get first instance from current pool and emit it
            IRecyclable recyclable = pool.FirstOrDefault();
            if (recyclable != null)
            {
                pool.Remove(recyclable);
                recyclable.Refresh();
                return recyclable;
            }
            // If pool was empty, but we have Constructor provided - create new instance and emit it
            else if (producer != null)
            {

                recyclable = producer();
                recyclable.SetPool(this);
                recyclable.Refresh();
                return recyclable;
            }
            // Otherwise emit default value
            else
            {
                recyclable = default(IRecyclable);
                return recyclable;
            }
        }

        /// <summary>
        /// Put IRecyclable back into the pool
        /// </summary>
        /// <param name="instance"></param>
        public void Put(IRecyclable instance)
        {
            // If instance is null - do nothing
            if (instance == null)
                return;
            
            // If instance was in pool, but the pool is not this one
            if (instance.GetPool() != null && instance.GetPool().ID() != this.ID())
            {
                // If instance is Unity Object - Destroy it
                if (instance is UnityEngine.Object)
                    UnityEngine.Object.Destroy((UnityEngine.Object) instance);
                // If Destructor is proveded - destruct object
                else if (destroyer != null)
                    destroyer(instance);
                // Otherwise make instance default value
                else
                    instance = default(IRecyclable);
                return;
            }
            
            // If instance was from this pool - recycle it and put back into the pool
            instance.Recycle();
            pool.Add(instance);
        }
    }
}
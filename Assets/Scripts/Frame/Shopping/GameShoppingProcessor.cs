﻿using Data;

namespace Frame
{
    public class GameShoppingProcessor : ShoppingProcessor
    {
        public override void Process(ShoppingEntity good)
        {
            base.Process(good);
            PopupFactory.Instance.Popup(
                PopupFactory.Instance.MakePopupOk(
                    "Shop!",
                    "You have bought a "+good.shopping_name,
                    () =>
                    {
                        PopupFactory.Instance.Popup(
                            null,
                            false);
                    }
                    ),
                true
                );
        }
    }
}
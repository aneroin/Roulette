﻿//#define SHOPPING_ENABLE

using System;
using System.Collections.Generic;
using Data;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
#if SHOPPING_ENABLE
using UnityEngine.Purchasing;
#endif

namespace Frame
{
    public class InAppPurchase : MonoBehaviour 
#if SHOPPING_ENABLE 
        , IStoreListener 
#endif
    {
        #region Singleton
        
        private static InAppPurchase _Instance;
        public static  InAppPurchase Instance
        {
            get { return _Instance; }
        }
        
        
        void Awake()
        {
            if (_Instance != null && _Instance != this)
            {
                DestroyImmediate(this);
            }
            else
            {
                _Instance = this;
                DontDestroyOnLoad(this);
            }
            return;
            
        }
        
        #endregion
        
#if SHOPPING_ENABLE 

        private Action<Product, PurchaseFailureReason> _failureHandler;
        private Action<Product> _completeHandler;
        
        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.
        
        /// <summary>
        /// Here you can define your products IDs
        /// </summary>
        private static Dictionary<string, string> purchaseableItems = new Dictionary<string, string>()
        {
            {"BuyGoldSmall", "com.talgame.spinner.coins1"},
            {"BuyGoldMedium", "com.talgame.spinner.coins2"},
            {"BuyGoldBig", "com.talgame.spinner.coins3"}
        };
        
        void Start()
        {
            // If we haven't set up the Unity Purchasing reference
            if (m_StoreController == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }
        }
        
        public void InitializePurchasing() 
        {
            // If we have already connected to Purchasing ...
            if (IsInitialized())
            {
                // ... we are done here.
                return;
            }
            
            // Create a builder, first passing in a suite of Unity provided stores.
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            
            // Add a product to sell / restore by way of its identifier, associating the general identifier
            // with its store-specific identifiers.
            builder.AddProduct(purchaseableItems["BuyGoldSmall"], ProductType.Consumable);
            builder.AddProduct(purchaseableItems["BuyGoldMedium"], ProductType.Consumable);
            builder.AddProduct(purchaseableItems["BuyGoldBig"], ProductType.Consumable);
            // Continue adding the non-consumable product.
            UnityPurchasing.Initialize(this, builder);
        }
        
         private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        public static Product GetItemmById(string id)
        {
            if (!purchaseableItems.ContainsKey(id))
                throw new KeyNotFoundException("Item "+id+" is not present in a products catalog");
            
            Product item = m_StoreController.products.WithID(purchaseableItems[id]);
            
            if (item == null)
                throw new ArgumentNullException("Item "+id+" points to the product "+purchaseableItems[id]+" which is unavaiable");

            return item;
        }


        public static void BuyItemByIdStatic(string id, Action<Product> onComplete, Action<Product,PurchaseFailureReason> onFailure)
        {
            _Instance.BuyItemById(id, onComplete, onFailure);
           
        }
        
        public static void BuyItemByShoppingEntityStatic(ShoppingEntity entity, Action<Product> onComplete, Action<Product,PurchaseFailureReason> onFailure)
        {
            _Instance.BuyItemByShoppingEntity(entity, onComplete, onFailure);
        }
        
        public void BuyItemById(string id, Action<Product> onComplete, Action<Product,PurchaseFailureReason> onFailure)
        {
            if (onFailure != null)
                _Instance._failureHandler = onFailure;
            if (onComplete != null)
                _Instance._completeHandler = onComplete;
            if (purchaseableItems.ContainsKey(id))
                BuyProductID(purchaseableItems[id]);
            else
                onFailure(null, PurchaseFailureReason.ProductUnavailable);
        }
        
        public void BuyItemByShoppingEntity(ShoppingEntity entity, Action<Product> onComplete, Action<Product,PurchaseFailureReason> onFailure)
        {
            if (onFailure != null)
                _Instance._failureHandler = onFailure;
            if (onComplete != null)
                _Instance._completeHandler = onComplete;
            if (purchaseableItems.ContainsKey(entity.id))
                BuyProductID(purchaseableItems[entity.id]);
            else
                onFailure(null, PurchaseFailureReason.ProductUnavailable);
        }
        
        void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID(productId);
                
                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    if (_Instance._failureHandler != null)
                    {
                        _Instance._failureHandler(null, PurchaseFailureReason.ProductUnavailable);
                    }
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                if (_Instance._failureHandler != null)
                {
                    _Instance._failureHandler(null, PurchaseFailureReason.PurchasingUnavailable);
                }
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }
        
        
        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }
            
            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer || 
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");
                
                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) => {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }
        
        
        //  
        // --- IStoreListener
        //
        
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            Debug.Log("OnInitialized: PASS");
            
            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;
        }
        
        
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }
        
        
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
        {
            if (String.Equals(args.purchasedProduct.definition.id, purchaseableItems["BuyGoldSmall"], StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
                GameScope.Instance.GetComponent<GameData>().Gold.Earn(1250);
                if (_Instance._completeHandler != null)
                {
                    _Instance._completeHandler(args.purchasedProduct);
                }
            }
            else if (String.Equals(args.purchasedProduct.definition.id, purchaseableItems["BuyGoldMedium"], StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
                GameScope.Instance.GetComponent<GameData>().Gold.Earn(3500);
                if (_Instance._completeHandler != null)
                {
                    _Instance._completeHandler(args.purchasedProduct);
                }
            }
            else if (String.Equals(args.purchasedProduct.definition.id, purchaseableItems["BuyGoldBig"], StringComparison.Ordinal))
            {
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
                // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
                GameScope.Instance.GetComponent<GameData>().Gold.Earn(7500);
                if (_Instance._completeHandler != null)
                {
                    _Instance._completeHandler(args.purchasedProduct);
                }
            }
            // Or ... an unknown product has been purchased by this user. Fill in additional products here....
            else 
            {
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
                if (_Instance._failureHandler != null)
                {
                    _Instance._failureHandler(args.purchasedProduct, PurchaseFailureReason.ProductUnavailable);
                }
            }
            
            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }
        
        
        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (_Instance._failureHandler != null)
            {
                _Instance._failureHandler(product, failureReason);
            }
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }
  
#endif
  
    }
}
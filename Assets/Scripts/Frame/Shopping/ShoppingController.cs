﻿//#define SHOPPING_ENABLE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Data;
using UnityEngine;

#if SHOPPING_ENABLE
using UnityEngine.Purchasing;
#endif

namespace Frame
{
    public class ShoppingController : MonoBehaviour
    {
        public List<ShoppingEntity> Goods;
        private GameData data;

        private ShoppingProcessor _gameCurrencyShoppingProcessor;
        private ShoppingProcessor _realCurrencyShoppingProcessor;

        void Start()
        {
            data = GameScope.Instance.GetComponent<GameData>();
            
            _gameCurrencyShoppingProcessor = new GameShoppingProcessor();
            
#if SHOPPING_ENABLE
            foreach (var good in Goods.Where(g => g.realmoney))
            {
                try
                {
                    Product prod = InAppPurchase.GetItemmById(good.id);
                    good.name = prod.metadata.localizedTitle;
                    good.description = prod.metadata.localizedDescription;
                    good.price = (long) (prod.metadata.localizedPrice * 100m);
                    good.currency = prod.metadata.isoCurrencyCode;
                }
                catch
                {
                    Debug.LogWarning("Product "+good.id+" is listed as real money purchaseable, but not configured in the Unity IAP");
                }
            }
#endif
        }
        
        public ShoppingEntity GetById(string id)
        {
            return Goods.FirstOrDefault(g => g.shopping_id == id);
        }
        
        //TODO: Price currency selector [Gold, Crystals, Real]
        public bool ProcessShopping(ShoppingEntity good)
        {
            if (good.shopping_prices.Count(p => p.PriceCurrency == Price.Currency.Real) == 0)
                if (data.Gold.Pay(good.shopping_prices.Single(p => p.PriceCurrency == Price.Currency.Gold).PriceAmount))
                {
                    SoundManager.Play("purchase", false, 1f, 1f);
                    _gameCurrencyShoppingProcessor.Process(good);
                }
                else
                {
                    PopupFactory.Instance.Popup(
                        PopupFactory.Instance.MakePopupOk(
                            "WARNING", "NOT ENOUGH COINS!",
                            Resources.Load<Sprite>("Sprites/Misc/coins"),
                            () =>
                            {
                                PopupFactory.Instance.Popup(null, false);
                            }
                        ),
                        true
                    );
                }
#if SHOPPING_ENABLE  
            else
            {              
                InAppPurchase.BuyItemByShoppingEntityStatic(good,
                    (product) =>
                    {
                        PopupFactory.Instance.Popup(
                            PopupFactory.Instance.MakePopupYay(
                                "YOU HAVE PURCHASED", product.metadata.localizedTitle.ToUpper()+"!",
                                Resources.Load<Sprite>("Sprites/Misc/coins"),
                                () =>
                                {
                                    PopupFactory.Instance.Popup(null, false);
                                }
                            ),
                            true
                        );
                        
                    },
                    (product, reason) =>
                    {
                        PopupFactory.Instance.Popup(
                            PopupFactory.Instance.MakePopupOk(
                                "OOPS!", String.Format("SOMETHING WENT WRONG WHILE PURCHASING {0}...", product!=null ? product.metadata.localizedTitle : "WE ARE SORRY"),
                                Resources.Load<Sprite>("Sprites/Misc/coins"),
                                () =>
                                {
                                    PopupFactory.Instance.Popup(null, false);
                                }
                            ),
                            true
                        );
                    }
                    
                    );
                return true;
            }
#endif
            return false;   
        }
        
    }
}
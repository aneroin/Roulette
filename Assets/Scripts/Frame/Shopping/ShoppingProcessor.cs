﻿using Data;
using UnityEngine;

namespace Frame
{
    public abstract class ShoppingProcessor
    {
        public virtual void Process(ShoppingEntity good)
        {
            Debug.LogWarning("You have bought a "+good.shopping_name);
        }
    }
}
﻿#define SOUND_ENABLE

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Frame
{
    public class SoundManager : MonoBehaviour
    {
        public static float MAX_BG_VOLUME = 0.5f;
        public static float MAX_FX_VOLUME = 1f;

        protected static float _MASTER_BG_VOLUME;

        public static float MasterBgVolume
        {
            get { return _MASTER_BG_VOLUME; }
            set { 
                if (_MASTER_BG_VOLUME == value) 
                    return;
                else
                {
                    _MASTER_BG_VOLUME = value;
                    instance.backgroundTheme.volume = _MASTER_BG_VOLUME * _MASTER_ALL_VOLUME;
                }
            }
        }
        
        protected static float _MASTER_FX_VOLUME;

        public static float MasterFxVolume
        {
            get { return _MASTER_FX_VOLUME; }
            set { if (_MASTER_FX_VOLUME == value) 
                    return;
                else
                {
                    foreach (var fx in instance.audio_pool)
                    {
                        fx.source.volume = ( fx.source.volume / _MASTER_FX_VOLUME) * value;
                    }
                    _MASTER_FX_VOLUME = value;
                    
                }}
        }
        
        protected static float _MASTER_ALL_VOLUME = 1f;


        [Range(0f,1f)]
        public float Master_BG_Volume;
        [Range(0f,1f)]
        public float Master_FX_Volume;
        
        private static SoundManager instance;

        private GameObject audiosources;
        
        public List<SoundPoolEntity> audio_pool;
        
        public List<SoundEntity> sounds;

        public AudioSource backgroundTheme;

        private Coroutine crossfading;
        
        void Awake()
        {
            if (instance != null && instance != this)
                Destroy(this);
            else
            {
                instance = this;
                _MASTER_BG_VOLUME = Master_BG_Volume;
                _MASTER_FX_VOLUME = Master_FX_Volume;
                audiosources = new GameObject("Audio Sources");
                audiosources.transform.parent = transform;
                audio_pool = new List<SoundPoolEntity>();
                backgroundTheme = audiosources.AddComponent<AudioSource>();
            }
        }

        private void FixedUpdate()
        {
            MasterBgVolume = Master_BG_Volume;
            MasterFxVolume = Master_FX_Volume;
        }

        public static void Play(string sound, bool looped, float pitch, float volume)
        {    
#if SOUND_ENABLE
            AudioSource source = looped ? instance.backgroundTheme : instance.PoolEmit();
            
            source.loop = looped;
            source.pitch = pitch;
            
            if (looped)
            {
                source.volume = Mathf.Clamp(volume, 0f, MAX_BG_VOLUME) * _MASTER_BG_VOLUME * _MASTER_ALL_VOLUME;
                if (instance.crossfading != null)
                    instance.StopCoroutine(instance.crossfading);
                instance.crossfading = instance.StartCoroutine(instance.CrossFadeSound(source, instance.sounds.Single(s => s.name == sound).clip));
            }
            else
            {
                source.volume = Mathf.Clamp(volume, 0f, MAX_FX_VOLUME) * _MASTER_FX_VOLUME * _MASTER_ALL_VOLUME;
                try
                {
                    source.clip = instance.sounds.SingleOrDefault(s => s.name == sound).clip;
                } catch
                {
                    Debug.LogError("Clip named "+sound+" can not be loaded");
                }
                source.Play();
                instance.StartCoroutine(
                    instance.ExecuteDelayed(
                        source.clip.length, () => { instance.PoolReceive(source); }
                    )
                );
            }
#endif
        }

        AudioSource PoolEmit()
        {
            
            SoundPoolEntity soundPoolEntity = audio_pool.FirstOrDefault(a => a.free);
  
            if (soundPoolEntity == null)
            {
                soundPoolEntity = new SoundPoolEntity(audiosources.AddComponent<AudioSource>());
                audio_pool.Add(soundPoolEntity);
            }
            soundPoolEntity.free = false;
            return soundPoolEntity.source;
        }

        void PoolReceive(AudioSource source)
        {
            SoundPoolEntity soundPoolEntity = audio_pool.SingleOrDefault(a => a.source == source);
            
            if (soundPoolEntity==null)
                Destroy(source);
            else
            {
                soundPoolEntity.free = true;
            }
        }

        IEnumerator ExecuteDelayed(float delay, Action action)
        {
            yield return new WaitForSeconds(delay);
            action();   
        }

        IEnumerator CrossFadeSound(AudioSource source, AudioClip clip)
        {
            if (source.clip == null)
            {
                source.clip = clip;
                source.Play();
            }
            else
            {
                float v = source.volume;
                while (v > 0f)
                {
                    source.volume = v;
                    v -= Time.deltaTime * 5f;
                    yield return new WaitForEndOfFrame();
                }

                source.volume = 0f;
                source.Stop();
                source.clip = clip;
                source.Play();

                while (v < MAX_BG_VOLUME * _MASTER_BG_VOLUME * _MASTER_ALL_VOLUME)
                {
                    source.volume = v;
                    v += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                source.volume = MAX_BG_VOLUME * _MASTER_BG_VOLUME * _MASTER_ALL_VOLUME;
            }
            yield return true;
        }


        [Serializable]
        public class SoundPoolEntity
        {
            public bool free;
            public AudioSource source;

            public SoundPoolEntity(AudioSource source)
            {
                this.source = source;
                free = true;
            }
        }
    }
}
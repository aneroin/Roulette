﻿using System.Collections.Generic;

namespace Frame.Storage
{
    public interface IPersister
    {
        void Write(ICollection<KeyValuePair<string, object>> data);
        ICollection<KeyValuePair<string, object>> Read();
    }
}
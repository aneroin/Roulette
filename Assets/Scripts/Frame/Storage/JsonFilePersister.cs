﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using UnityEngine;

namespace Frame.Storage
{
    public class JsonFilePersister : IPersister
    {
        protected string filePath;
        protected string fileName;

        public JsonFilePersister(string filePath = "/DataStorage", string fileName = "data.json")
        {
            this.filePath = Application.persistentDataPath + filePath;
            this.fileName = "/" + fileName + ".json";

            if (!System.IO.Directory.Exists(this.filePath))
            {
                System.IO.Directory.CreateDirectory(this.filePath);
            }

        }
        
        public void Write(ICollection<KeyValuePair<string, object>> data)
        {
            KVPairWrapper[] payload = new KVPairWrapper[data.Count];
            int i = 0;
            foreach (var kvp in data)
            {
                payload[i] = new KVPairWrapper(kvp);
                i++;
            }
            KVCollectionWrapper wrapper = new KVCollectionWrapper(payload);
            string content = JsonUtility.ToJson(wrapper, true);
            System.IO.File.WriteAllText(filePath + fileName, content);
        }

        public ICollection<KeyValuePair<string, object>> Read()
        {
            if (!System.IO.File.Exists(filePath + fileName))
            {
                Debug.LogError("no file found at "+ filePath + fileName);
                return new Dictionary<string, object>();
            }
            
            string content = System.IO.File.ReadAllText(filePath + fileName);
            KVCollectionWrapper wrapper =
                JsonUtility.FromJson<KVCollectionWrapper>(content);
            Dictionary<string, object> data = new Dictionary<string, object>();
            foreach (var kvpw in wrapper.collection)
            {
                KeyValuePair<string, object> kvp = kvpw.read();
                data.Add(kvp.Key, kvp.Value);
            }
            return data;
        }
    }

    [System.Serializable]
    public class KVCollectionWrapper
    {
        public KVPairWrapper[] collection;

        public KVCollectionWrapper(KVPairWrapper[] c)
        {
            this.collection = c;
        }
    }

    [System.Serializable]
    public class KVPairWrapper
    {
        public string key;
        public string val;

        public KVPairWrapper()
        {
        }

        public KVPairWrapper(string k, object v)
        {
            key = k;
            val = Convert.ToBase64String((byte[]) v);
        }
        
        public KVPairWrapper(KeyValuePair<string, object> kv)
        {
            MemoryStream stream = new MemoryStream(1024);
            new BinaryFormatter().Serialize(stream, kv.Value);
            key = kv.Key;
            val = Convert.ToBase64String(stream.GetBuffer());
        }
        
        public KeyValuePair<string, object> read()
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream(Convert.FromBase64String(val).Length);
            stream.Write(Convert.FromBase64String(val), 0, Convert.FromBase64String(val).Length);
            stream.Seek(0, 0);
            return new KeyValuePair<string, object>(key, new BinaryFormatter().Deserialize(stream));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Frame.Storage
{
    public class KVDB : KeyValueStorage
    {
        protected static KVDB _instance;

        public static KVDB Instance
        {
            get { return _instance ?? (_instance = new KVDB()); }
        }

        public static T Get<T>(string key)
        {
            object candidate = ((KeyValueStorage) Instance).Get(key);
            return (candidate is T ? (T) candidate : default(T));
        }

        public static void Set<T>(string key, T value)
        {
            ((KeyValueStorage) Instance).Set(key, value);
        }

        public static void Delete(string key)
        {
            ((KeyValueStorage) Instance).Delete(key);
        }

        public static bool ContainsKey(string key)
        {
           return ((KeyValueStorage) Instance).ContainsKey(key);
        }

        public static IEnumerable<T> Select<T>(Func<KeyValuePair<string, object>, bool> selector)
        {
            return Instance.storage.Where(selector).Select((c) => { return c.Value; }).Cast<T>().ToList();
        }
        
        public static T First<T>(Func<KeyValuePair<string, object>, bool> selector)
        {
            object candidate = Instance.storage.FirstOrDefault(selector).Value;
            return candidate is T ? (T) candidate : default(T);
        }

        public static void PersistInDebug()
        {
            _instance.Persist((store) =>
            {
                Debug.LogWarning("KVDB contains:");
                foreach (var data in store)
                {
                    Debug.LogWarning(data.Key+" => "+data.Value);
                }
                
            });
        }
    }
}
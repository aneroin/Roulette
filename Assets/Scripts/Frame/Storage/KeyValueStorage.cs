﻿using System;
using System.Collections.Generic;

namespace Frame.Storage
{
    public abstract class KeyValueStorage
    {
        protected ICollection<KeyValuePair<string, object>> storage = new Dictionary<string, Object>();
        
        protected Dictionary<string, Object> storageDictionary
        {
            get { return storage as Dictionary<string, Object> ; }
        }

        public Object Get(string key)
        {
            return storageDictionary.ContainsKey(key) ? storageDictionary[key] : null;
        }
        
        public void Set(string key, Object value)
        {
            if (storageDictionary.ContainsKey(key))
                storageDictionary.Remove(key);
            
            storageDictionary.Add(key, value);
        }

        public void Delete(string key)
        {
            storageDictionary.Remove(key);
        }

        public bool ContainsKey(string key)
        {
            return storageDictionary.ContainsKey(key);
        }

        public void Persist(Action<ICollection<KeyValuePair<string, object>>> persister)
        {
            persister(storage);
        }
        
        public void Retrieve(Action<ICollection<KeyValuePair<string, object>>> persister)
        {
            persister(storage);
        }
        
        
        public void PersistSession(IPersister persister)
        {
            persister.Write(storage);
        }

        public void RetrieveSession(IPersister persister)
        {
            storage = persister.Read();
        }
    }
}
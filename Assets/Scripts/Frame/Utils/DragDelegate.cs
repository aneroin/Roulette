﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Frame
{
    public class DragDelegate : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [SerializeField]
        private Vector2 startDragPosition;
        [SerializeField]
        private Vector2 totalDragPosition;
        [SerializeField]
        private Vector2 endDragPosition;
        
        [Flags]
        public enum DragListenerType
        {
            none = 0,
            beginDrag = 1,
            drag = 2,
            endDrag = 4
        }
        
        protected Action<Vector2> DragHandler;
        protected Action<Vector2> DragBeginHandler;
        protected Action<Vector2> DragEndHandler;
        
        public DragDelegate Bind(Action<Vector2> callback)
        {
            DragHandler += callback;
            return this;
        }
        
        public DragDelegate Bind(Action<Vector2> callback, DragListenerType dragType)
        {
            if ((dragType & DragListenerType.drag) == DragListenerType.drag)
                DragHandler += callback;
            if ((dragType & DragListenerType.beginDrag) == DragListenerType.beginDrag)
                DragBeginHandler += callback;
            if ((dragType & DragListenerType.endDrag) == DragListenerType.endDrag)
                DragEndHandler += callback;
            
            return this;
        }

        public DragDelegate UnbindAll()
        {
            DragHandler = null;
            DragBeginHandler = null;
            DragEndHandler = null;
            return this;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            startDragPosition = eventData.position;
            totalDragPosition = Vector2.zero;
            if (DragBeginHandler != null)
                DragBeginHandler(startDragPosition);
                
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            endDragPosition = eventData.position;
            if (DragEndHandler != null)
                DragEndHandler(endDragPosition);
        }

        public void OnDrag(PointerEventData eventData)
        {
            totalDragPosition += eventData.delta;
            if (DragHandler != null)
                DragHandler(totalDragPosition);
        }
    }
}
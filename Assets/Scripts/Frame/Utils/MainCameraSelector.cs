﻿using UnityEngine;

namespace Frame
{
    [RequireComponent(typeof(Camera))]
    public class MainCameraSelector : MonoBehaviour
    {
        void Awake()
        {
            Camera.SetupCurrent(GetComponent<Camera>());
        }
    }
}
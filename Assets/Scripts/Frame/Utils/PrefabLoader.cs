﻿using UnityEngine;

namespace Frame
{
    public class PrefabLoader : MonoBehaviour
    {
        public GameObject Prefab;

        public void LoadPrefab()
        {
            Transform parent = transform.parent;
            Vector3 position = transform.position;
            Quaternion rotation = transform.rotation;
            int hierarchy = transform.GetSiblingIndex();
            string name = gameObject.name;

            GameObject fresh = Instantiate(Prefab, parent);

            fresh.transform.position = position;
            fresh.transform.rotation = rotation;
            fresh.transform.SetSiblingIndex(hierarchy);
            fresh.gameObject.name = name;
            
            DestroyImmediate(this.gameObject);
        }
        
        #if UNITY_EDITOR 
        
        [UnityEditor.MenuItem("Utils/LoadPrefabs")]
        public static void LoadPrefabs()
        {
            PrefabLoader[] loaders = FindObjectsOfType<PrefabLoader>();

            foreach (var loader in loaders)
            {
                loader.LoadPrefab();
            }
            
        }
        
        #endif
    }
}
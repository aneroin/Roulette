﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace Frame
{
    public class RandomBroker : MonoBehaviour
    {
        Dictionary<String, Random> randoms = new Dictionary<String, Random>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"> name of adhoc random</param>
        /// <param name="seed"> int value != 0</param>
        /// <returns></returns>
        public Random GetRandomAdhoc(string name = "AdHoc", int seed = 0)
        {
            if (!randoms.ContainsKey(name))
            {
                if (seed == 0)
                {
                    if (!PlayerPrefs.HasKey("RandomSeed:" + name))
                    {
                        Guid guid = Guid.NewGuid();
                        seed = guid.GetHashCode();
                        PlayerPrefs.SetInt("RandomSeed:" + name, seed);
                    }
                    else
                    {
                        seed = PlayerPrefs.GetInt("RandomSeed:" + name);
                    }
                } 

                Random r = new Random(seed);
                randoms.Add(name, r);
            }
          
            return randoms[name];
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"> name of adhoc random</param>
        /// <param name="seed"> int value != 0</param>
        /// <returns></returns>
        public void AdhocToState(string name = "AdHoc", int seed = 0)
        {
            if (randoms.ContainsKey(name))
            {
                if (seed != 0)
                    seed = PlayerPrefs.GetInt("RandomSeed:"+name);
                randoms[name] = new Random(seed);
            }
        }
        
        public Random GetRandom(string name)
        {
            if (!randoms.ContainsKey(name))
            {
                int seed;
                if (!PlayerPrefs.HasKey("RandomSeed:" + name))
                {
                    Guid guid = Guid.NewGuid();
                    seed = guid.GetHashCode();
                    PlayerPrefs.SetInt("RandomSeed:" + name, seed);
                }
                else
                {
                    seed = PlayerPrefs.GetInt("RandomSeed:" + name);
                }

                Random r = new Random(seed);
                randoms.Add(name, r);
            }
          
            return randoms[name];
        }

        public void ToInitialState(string name)
        {
            if (randoms.ContainsKey(name))
            {
                int seed;
                seed = PlayerPrefs.GetInt("RandomSeed:"+name);
                randoms[name] = new Random(seed);
            }
        }
    }
}
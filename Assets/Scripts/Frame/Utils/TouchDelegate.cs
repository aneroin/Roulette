﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Frame
{
    public class TouchDelegate : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler
    {
        [Flags]
        public enum TouchListenerType
        {
            none = 0,
            pointerDown = 1,
            pointerUp = 2,
            pointerClick = 4,
            pointerHold = 8,
            pointerDownHold = 9,
            pointerHoldUp = 10,
            pointerHoldClick = 12
        }

        public TouchListenerType Type;
        
        protected Action Handler;

        protected bool holded;
        
        public TouchDelegate Bind(Action callback)
        {
            Handler += callback;
            Type = TouchListenerType.pointerClick;
            return this;
        }
        
        public TouchDelegate Bind(Action callback, TouchListenerType touchType)
        {
            Handler += callback;
            Type = touchType;
            return this;
        }

        public TouchDelegate UnbindAll()
        {
            Handler = null;
            return this;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if ((Type & TouchListenerType.pointerClick) == TouchListenerType.pointerClick)
                if (Handler != null)
                    Handler();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if ((Type & TouchListenerType.pointerHold) == TouchListenerType.pointerHold)
                holded = true;
            
            if ((Type & TouchListenerType.pointerDown) == TouchListenerType.pointerDown)
            {
                if (Handler != null)
                    Handler();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        { 
            if ((Type & TouchListenerType.pointerHold) == TouchListenerType.pointerHold)
                holded = false;
            
            if ((Type & TouchListenerType.pointerUp) == TouchListenerType.pointerUp)
            {
                if (Handler != null)
                    Handler();
            }
        }

        private void Update()
        {
            if (holded)
            {
                if (Handler != null)
                    Handler();
                if ((Type & TouchListenerType.pointerClick) == TouchListenerType.pointerClick)
                    holded = false;
            }
        }
    }
}
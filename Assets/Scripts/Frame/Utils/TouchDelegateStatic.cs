﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Frame
{
    public class TouchDelegateStatic : TouchDelegate
    {
        public UnityEvent OnTouched;
        
        public void Awake()
        {
            Handler = OnTouched.Invoke;
        }
        
        public TouchDelegateStatic Bind(Action callback)
        {
            OnTouched.AddListener(new UnityAction(callback));
            Handler = OnTouched.Invoke;
            Type = TouchListenerType.pointerClick;
            return this;
        }
        
        public TouchDelegateStatic Bind(Action callback, TouchListenerType touchType)
        {
            OnTouched.AddListener(new UnityAction(callback));
            Handler = OnTouched.Invoke;
            Type = touchType;
            return this;
        }
        
        public TouchDelegateStatic UnbindAll()
        {
            OnTouched.RemoveAllListeners();
            Handler = null;
            return this;
        }
    }
}
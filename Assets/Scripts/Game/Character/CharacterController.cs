﻿using System;
using System.Collections;
using Data;
using Frame;
using Frame.EventsPipe;
using Game.Revolver;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Character
{
    public class CharacterController : MonoBehaviour
    {
        [SerializeField]
        private int playerNumber;
        [SerializeField]
        private int health;

        public int Health
        {
            get { return health; }
            set { health = value; PipeHost.SendEvent(new PipeEvent<int>(health, "health_changed_"+playerNumber)); }
        }

        private RoundEntity _round;

        private Animator _animator;

        void Start()
        {
            _animator = GetComponent<Animator>();
            _round = GameScope.Get<GameData>().Get<RoundEntity>("Round");
            PipeHost.Subscribe<RevolverController.DrumShotData>(callback: HandleShot, tag: "revolver_shot");
            PipeHost.Subscribe<int>(callback:MakeTurnForAi, tag:"turn_changed");
            Health = 50;
        }

        void HandleShot(RevolverController.DrumShotData shotData)
        {
            if (_round.Turn == playerNumber)
            {
                if (shotData.HasBullet)
                {
                    _animator.SetTrigger("trigger_shot_success");
                }
                else
                {
                    _animator.SetTrigger("trigger_shot_fail");
                }

            }
            else
            {
                if (shotData.HasBullet)
                {
                    Health -= 10;
                    _animator.SetTrigger("trigger_hit_success");
                }
                else
                {
                    _animator.SetTrigger("trigger_hit_fail");
                }
            }
        }

        void MakeTurnForAi(int turn)
        {
            if (turn == 1)
                StartCoroutine(
                    Delay(2f, () =>
                    {
                        PipeHost.SendEvent(new PipeEvent<int>(data: Random.Range(0, 6), tag: "revolver_spin"));
                    })
                );
        }

        IEnumerator Delay(float delay, Action callback)
        {
            yield return new WaitForSeconds(delay);
            callback();
            yield return true;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Data;
using Frame;
using Frame.EventsPipe;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Game.Revolver
{
    public class RevolverController : MonoBehaviour
    {
        public bool AI;
        
        //TODO: Dynamic control
        private const float Sector = 360f / 6f;

        public AnimationCurve RotationCurve;
        
        [Range(0.01f, 1f)]
        public float SecondsPerBullet;
        
        public RawImage Body;
        public RawImage Drum;
        public RawImage Hammer;
        public List<Image> BulletHoles;
        public List<bool> IsBullet;
        
        private Coroutine spinning;

        public Button SpinButton;

        private int[] numbers;

        private bool ready;

        private RoundEntity round;

        void Start()
        {
            round = GameScope.Get<GameData>().Get<RoundEntity>("Round");
            PipeHost.Subscribe<int>(Rotate, tag: "revolver_spin");
            PipeHost.Subscribe<DrumShotData>(StrikeHammer, tag: "revolver_shot");
            // Player controls by Button
            // Ai controls from character controller, so no need for subscription
            if (!AI)
            {
                TouchDelegate touchDelegate = SpinButton.gameObject.AddComponent<TouchDelegate>();
                touchDelegate.Bind(() =>
                {
                    if (round.Turn == 0)
                        PipeHost.SendEvent(new PipeEvent<int>(data: Random.Range(0, 6), tag: "revolver_spin"));
                }, TouchDelegate.TouchListenerType.pointerClick);
            }
            numbers = new int[] {0, 1, 2, 3, 4, 5};
            ResetDrum();
            round.Turn = 0;
            ready = true;
        }

        /// <summary>
        /// Reset bullets in a drum and proceed to the next turn
        /// </summary>
        void ResetDrum()
        {
            float backtrack = Drum.transform.localRotation.eulerAngles.z;
            StartCoroutine(DrumSpin(null, backtrack, 1f, false));
            //Clear Slots
            for (int i = 0; i < 6; i++)
            {
                BulletHoles[i].GetComponent<Animation>().Stop();
                BulletHoles[i].color = new Color(1f,1f,1f,0f);
                IsBullet[i] = false;
            }
            
            //ShufflePositions
            for (int i = 0;i < numbers.Length;++i)
            {
                int randomIndex = Random.Range(0, numbers.Length);
                int temp = numbers[randomIndex];
                numbers[randomIndex] = numbers[i];
                numbers[i] = temp;
            }
            
            //TODO: Externalize
            int bulletsCount = round.Bullets;
            for (int i = 0; i < bulletsCount; i++)
            {
                IsBullet[numbers[i]] = true;
                BulletHoles[numbers[i]].color = Color.white;
            }
            
            GameScope.Get<GameData>().Get<RoundEntity>("Round").Turn++;
        }
        
        /// <summary>
        /// Test which bullet is under hammer, shoot and then reset drum
        /// </summary>
        /// <param name="shot"></param>
        void StrikeHammer(DrumShotData shot)
        {
            if (shot.HasBullet)
            {
                if (AI && round.Turn == 0 || !AI && round.Turn == 1)
                    return;
                Debug.LogWarning("BulletShot");
                StartCoroutine(Strike(BulletHoles[shot.SlotNumber]));
            }
            else
            {
                if (AI && round.Turn == 0 || !AI && round.Turn == 1)
                    return;
                Debug.LogWarning("BulletEmpty");
                SoundManager.Play("clack", false, 1f, 1f);
                StartCoroutine(Delay(0.5f, ResetDrum));

            }
            
        }
        
        /// <summary>
        /// Spin our drum around and send event with indication whether there's a bullet under hammer or not
        /// </summary>
        /// <param name="count"></param>
        void Rotate(int count)
        {
            if (AI && round.Turn == 0 || !AI && round.Turn == 1)
                return;
            
            if (!ready)
                return;
            Debug.Log("Revolver Rotate by "+count);
            if (spinning != null)
                StopCoroutine(spinning);
            int recount = (count + 6);
            spinning = StartCoroutine(DrumSpin(new DrumShotData(count, IsBullet[count]), recount * Sector, recount * SecondsPerBullet));
        }

        IEnumerator DrumSpin(DrumShotData drumShot, float angle, float time, bool clockwise = true)
        {
            if (drumShot != null)
                SoundManager.Play("spin", false, 1f, 1f);
            ready = false;
            float timescale = 1f / time;
            float angleCurrent = Drum.transform.localRotation.eulerAngles.z;
            float angleTarget = clockwise ? angleCurrent + angle : angleCurrent - angle;
            float t = 0f;
            while (t < 1f)
            {
                Drum.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Lerp(angleCurrent, angleTarget, RotationCurve.Evaluate(t)));
                t += Time.deltaTime * timescale;
                yield return new WaitForEndOfFrame();
            }
            Drum.transform.localRotation = Quaternion.Euler(
                0f,
                0f, 
                angleTarget);

            if (drumShot != null)
                PipeHost.SendEvent(new PipeEvent<DrumShotData>(data: drumShot, tag: "revolver_shot"));
            else 
                ready = true;
            yield return false;
        }


        IEnumerator Strike(Image bullet)
        {
            Animation anim =  bullet.GetComponent<Animation>();
            ParticleSystem part = bullet.GetComponent<ParticleSystem>();
            
            if (part != null)
            {
                part.Play();
            }
            
            if (anim != null)
            {
                anim.Play("ShotCommon");
                SoundManager.Play("shot", false, 1f, 1f);
                yield return new WaitForSeconds(anim.GetClip("ShotCommon").length + 0.5f);
            }
            else
            {
                //TODO: Default Wait Time
                yield return new WaitForSeconds(1f);
            }
            
            ResetDrum();
            yield return true;
        }

        IEnumerator Delay(float delay, Action callback)
        {
            yield return new WaitForSeconds(delay);
            callback();
            yield return true;
        }
        
        
        public class DrumShotData
        {
            public DrumShotData(int slot, bool isbullet)
            {
                slotNumber = slot;
                hasBullet = isbullet;
            }
            
            private int slotNumber;
            public int SlotNumber
            {
                get { return slotNumber; }
            }
            
            private bool hasBullet;
            public bool HasBullet
            {
                get { return hasBullet; }
            }
        }
    }
}
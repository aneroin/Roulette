﻿using Frame.EventsPipe;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        private int playerNumber;

        public Slider slider;
        
        private string private_tag;
        
        void Start()
        {
            slider.value = slider.maxValue;
            private_tag = "health_changed_" + playerNumber;
            PipeHost.Subscribe<int>(UpdateHealth, private_tag);
        }

        void UpdateHealth(int health)
        {
            slider.value = health;
        }
    }
}
﻿using System;
using Frame;
using UnityEngine;
using UnityEngine.UI;

public abstract class Popup : MonoBehaviour
{
    public GameObject content;
    public Text Text_Caption;
    public Text Text_Content;
    
    public string _caption;
    public string _content;

    public Image _picture;
    
    protected Action _onClosed;

    public virtual void Close()
    {
        content.SetActive(false);
        if (_onClosed!=null)
            _onClosed();
    }
    
    public virtual void Show()
    {
        content.SetActive(true);
        Text_Caption.text = _caption;
        Text_Content.text = _content;
        SoundManager.Play("popup", false, 1f, 1f);
    }

    public void OnClosed(Action callback)
    {
        _onClosed = callback;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Data;
using UnityEngine;

public class PopupFactory : MonoBehaviour
{
    
    #region Singleton
    
    protected static PopupFactory _Instance;
    
    public static PopupFactory Instance
    {
        get { return _Instance; }
    }
    
    public void Awake()
    {
        if (_Instance != null && _Instance != this)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
    
        _Instance = this;
        
        if (_cache == null)
            _cache = new Dictionary<string, GameObject>();
        if (!_cache.ContainsKey(POPUPOK))
            _cache.Add(POPUPOK,Resources.Load<GameObject>("Prefabs/UI/"+POPUPOK));
        if (!_cache.ContainsKey(POPUPYESNO))
            _cache.Add(POPUPYESNO,Resources.Load<GameObject>("Prefabs/UI/"+POPUPYESNO));
        if (!_cache.ContainsKey(POPUPYAY))
            _cache.Add(POPUPYAY,Resources.Load<GameObject>("Prefabs/UI/"+POPUPYAY));

        Debug.Log("Popup Factory loaded");
    }
    
    #endregion
       
    #region cache

    private const string POPUPOK = "PopupOk";
    private const string POPUPYESNO = "PopupYesNo";
    private const string POPUPYAY = "PopupYay";
    
    private Dictionary<string, GameObject> _cache;
    
    #endregion
    
    #region Pool

    public GameObject Veil;
    
    public List<PopupYesNo> _pool_PopupYesNo;
    
    public PopupYesNo GetYesNo()
    {
        if (_pool_PopupYesNo == null)
            _pool_PopupYesNo = new List<PopupYesNo>();
        if (_pool_PopupYesNo.Count < 1)
            _pool_PopupYesNo.Add(NewPopupYesNo());
        PopupYesNo popup = _pool_PopupYesNo[0];
        _pool_PopupYesNo.RemoveAt(0);
        return popup;
    }
    
    public void PutYesNo(PopupYesNo popup)
    {
        if (_pool_PopupYesNo == null)
            _pool_PopupYesNo = new List<PopupYesNo>();
        _pool_PopupYesNo.Add(popup);
    }
    
    public List<PopupOk> _pool_PopupOk;
    
    public PopupOk GetOk()
    {
        if (_pool_PopupOk == null)
            _pool_PopupOk = new List<PopupOk>();
        if (_pool_PopupOk.Count < 1)
            _pool_PopupOk.Add(NewPopupOk());
        PopupOk popup = _pool_PopupOk[0];
        _pool_PopupOk.RemoveAt(0);
        return popup;
    }
    
    public void PutOk(PopupOk popup)
    {
        if (_pool_PopupOk == null)
            _pool_PopupOk = new List<PopupOk>();
        _pool_PopupOk.Add(popup);
    }
    
    public List<PopupYay> _pool_PopupYay;
    
    public PopupYay GetYay()
    {
        if (_pool_PopupYay == null)
            _pool_PopupYay = new List<PopupYay>();
        if (_pool_PopupYay.Count < 1)
            _pool_PopupYay.Add(NewPopupYay());
        PopupYay popup = _pool_PopupYay[0];
        _pool_PopupYay.RemoveAt(0);
        return popup;
    }
    
    public void PutYay(PopupYay popup)
    {
        if (_pool_PopupYay == null)
            _pool_PopupYay = new List<PopupYay>();
        _pool_PopupYay.Add(popup);
    }
    
    #endregion
    
    private Queue<Popup> PopupQueue;

    public void Popup(Popup popup)
    {
        Popup(popup, false);
    }

    public void Popup(Popup popup, bool veiled)
    {
        if (Veil != null)
            Veil.SetActive(veiled);
        if (popup!=null)
            popup.Show();
    }
    
    public void PopupInQueue(IEnumerable<Popup> popups)
    {
        PopupQueue = new Queue<Popup>(popups.Count());
        foreach (Popup popup in popups)
            PopupQueue.Enqueue(popup);
        ProcessQueue();
    }

    void ProcessQueue()
    {
        Popup popup = PopupQueue.Dequeue();
        if (PopupQueue.Count > 0)
            popup.OnClosed(ProcessQueue);
        popup.Show();
    }

    
    /// <summary>
    ///  Use this to display a popup and let it be recycled later
    /// </summary>
    /// <param name="caption">string caption</param>
    /// <param name="content">string text of the popup</param>
    /// <param name="onOkAction">action on OK button pressed</param>
    /// <returns>created popup</returns>
    public Popup MakePopupOk(string caption, string content, Action onOkAction)
    {
        PopupOk popup = GetOk();
        popup.MakePopupOk(caption, content, null, onOkAction);
        popup.OnRecycle(PutOk);
        return popup;
    }
    
    /// <summary>
    /// Use this to display a popup and let it be recycled later
    /// </summary>
    /// <param name="caption">string caption</param>
    /// <param name="content">string text of the popup</param>
    /// <param name="picture">sprite to be displayed</param>
    /// <param name="onOkAction">action on OK button pressed</param>
    /// <returns>created popup</returns>
    public Popup MakePopupOk(string caption, string content, Sprite picture, Action onOkAction)
    {
        PopupOk popup = GetOk();
        popup.MakePopupOk(caption, content, picture, onOkAction);
        popup.OnRecycle(PutOk);
        return popup;
    }
    
    //TODO: Reimplement
    public Popup MakePopupYesNo(string caption, string content, Action onYesAction, Action onNoAction)
    {
        throw new NotImplementedException();
        PopupYesNo popup = GetYesNo();
        popup.MakePopupYesNo(caption, content, onYesAction, onNoAction);
        popup.OnRecycle(PutYesNo);
        return popup;
    }
    
    public Popup MakePopupYay(string caption, string content, Action onOkAction)
    {
        PopupYay popup = GetYay();
        popup.MakePopupYay(caption, content, null, onOkAction);
        popup.OnRecycle(PutYay);
        return popup;
    }
    
    public Popup MakePopupYay(string caption, string content, Sprite picture, Action onOkAction)
    {
        PopupYay popup = GetYay();
        popup.MakePopupYay(caption, content, picture, onOkAction);
        popup.OnRecycle(PutYay);
        return popup;
    }    
    
    #region actual constructors
    
    private PopupOk NewPopupOk()
    {
        GameObject go = Instantiate(_cache[POPUPOK]);
        go.transform.parent = transform;
        PopupOk popup = go.GetComponent<PopupOk>();
        return popup;
    }
    
    private PopupYesNo NewPopupYesNo()
    {      
        GameObject go = Instantiate(_cache[POPUPYESNO]);
        go.transform.parent = transform;
        PopupYesNo popup = go.GetComponent<PopupYesNo>();
        return popup;
    }
    
    private PopupYay NewPopupYay()
    {      
        GameObject go = Instantiate(_cache[POPUPYAY]);
        go.transform.parent = transform;
        PopupYay popup = go.GetComponent<PopupYay>();
        return popup;
    }
    
    #endregion
}

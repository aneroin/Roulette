﻿

using System;
using Frame;
using UnityEngine;
using UnityEngine.UI;

public class PopupOk : Popup
{   
    private Action _okAction;
    
    private Action<PopupOk> _recycle;
    
    public PopupOk MakePopupOk(string caption, string content, Sprite picture, Action okAction)
    {
        _caption = caption;
        _content = content;
        _okAction = okAction;
        if (picture != null)
        {
            _picture.sprite = picture;
            _picture.GetComponent<AspectRatioFitter>().aspectRatio =
                _picture.preferredWidth / _picture.preferredHeight;
        }
        else
        {
            _picture.gameObject.SetActive(false);
        }
        return this;
    }

    public void Trigger_ButtonOK()
    {
        SoundManager.Play("button", false, 1f, 1f);
        if (_okAction != null)
            _okAction();
        Close();
        Recycle();
    }
    
    public void OnRecycle(Action<PopupOk> recycle)
    {
        _recycle = recycle;
    }

    void Recycle()
    {
        _okAction = null;
        if (_recycle!=null)
            _recycle(this);
    }
    
}
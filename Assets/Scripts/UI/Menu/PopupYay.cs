﻿using System;
using Data;
using Frame;
using UnityEngine;
using UnityEngine.UI;

public class PopupYay : Popup
{    
    private Action _yayAction;
    
    private Action<PopupYay> _recycle;

    private bool _plainImgFlag;
    
    public PopupYay MakePopupYay(string caption, string content, Sprite picture, Action yayAction)
    {
        _plainImgFlag = true;
        _caption = caption;
        _content = content;
        _yayAction = yayAction;
        if (picture != null)
        {
            _picture.sprite = picture;
            _picture.GetComponent<AspectRatioFitter>().aspectRatio =
                _picture.preferredWidth / _picture.preferredHeight;
            (_picture.transform as RectTransform).SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 600f);
        }
        else
        {
            _picture.gameObject.SetActive(false);
        }
        return this;
    }
    

    public override void Show()
    {
        base.Show();

        if (!_plainImgFlag)
        {
            _picture.GetComponent<Animation>().Play("rotate");
            _picture.GetComponent<Animation>().Blend("fade");
        }
    }
    
    public void Trigger_ButtonYAY()
    {
        SoundManager.Play("button", false, 1f, 1f);
        if (_yayAction != null)
            _yayAction();
        Close();
        Recycle();
    }
    
    public void OnRecycle(Action<PopupYay> recycle)
    {
        _recycle = recycle;
    }

    void Recycle()
    {
        _picture.transform.rotation = Quaternion.identity;
        _picture.color = Color.white;
        _picture.GetComponent<Animation>().Stop();
        _yayAction = null;
        if (_recycle!=null)
            _recycle(this);
    }
    
}
﻿

using System;
using System.Security.Cryptography;
using Frame;
using UnityEngine;
using UnityEngine.UI;

public class PopupYesNo : Popup
{   
    private Action _yesAction;
    private Action _noAction;

    private Action<PopupYesNo> _recycle;
    
    public PopupYesNo MakePopupYesNo(string caption, string content, Action yesAction, Action noAction)
    {
        _caption = caption;
        _content = content;
        _yesAction = yesAction;
        _noAction = noAction;
        return this;
    }

    public void Trigger_ButtonYes()
    {
        SoundManager.Play("button", false, 1f, 1f);
        if (_yesAction != null)
            _yesAction();
        Close();
        Recycle();
    }
    
    public void Trigger_ButtonNo()
    {
        SoundManager.Play("button", false, 1f, 1f);
        if (_noAction != null)
            _noAction();
        Close();
        Recycle();
    }

    public void OnRecycle(Action<PopupYesNo> recycle)
    {
        _recycle = recycle;
    }

    void Recycle()
    {
        _yesAction = null;
        _noAction = null;
        if (_recycle!=null)
            _recycle(this);
    }
    
}
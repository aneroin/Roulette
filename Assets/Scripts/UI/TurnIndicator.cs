﻿using Frame.EventsPipe;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TurnIndicator : MonoBehaviour
    {
        private Image img;

        public Animation PlayerTurn;
        public Animation OpponentTurn;
        
        void Start()
        {
            img = GetComponent<Image>();
            PipeHost.Subscribe<int>(callback:TurnChanged, tag:"turn_changed");
        }

        void TurnChanged(int player)
        {
            img.color = player == 0 ? Color.blue : Color.red;
            (player == 0 ? PlayerTurn : OpponentTurn).Play();
        }
    }
}